

var showDialogButton = document.querySelector('#fake-translate')
var dialog = document.querySelector('dialog')

if (! dialog.showModal) {
  dialogPolyfill.registerDialog(dialog);
}

showDialogButton.addEventListener('click', function() {
  dialog.showModal();
});

dialog.querySelector('.close').addEventListener('click', function() {
  dialog.close();
});
